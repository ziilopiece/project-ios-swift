//
//  ViewController.swift
//  Latihan1
//
//  Created by Macbook Pro on 17/03/23.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var rocketButton: UIButton!
    @IBOutlet weak var doctorButton: UIButton!
    @IBOutlet weak var rockStarButton: UIButton!
    @IBOutlet weak var astronoutButton: UIButton!
    
    //MARK: -LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    //MARK: -Helpers
    
    func showAlert(with title: String, and subtitle: String){
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        present(alert, animated: true)
    }
    
    //MARK: -Actions

    @IBAction func rocketButtonTapped(_ sender: Any) {
        showAlert(with: "Your Job", and: "Rocket")
    }
    
    @IBAction func astronoutButtonTapped(_ sender: Any) {
        showAlert(with: "Your Job", and: "Astronout")
    }
    
    @IBAction func doctorButtonTapped(_ sender: Any) {
        showAlert(with: "Your Job", and: "Doctor")
    }
    
    @IBAction func rockStarButtonTapped(_ sender: Any) {
        showAlert(with: "Your Job", and: "Rock Star")
    }
    
}

